package servlet;

import java.util.*;

import javax.ejb.*;
import javax.persistence.*;


@Stateless
public class BrokerModelImpl implements BrokerModel {

	@PersistenceContext 
	private EntityManager em;

    /** Creates a new instance of BrokerModelImpl */
    public BrokerModelImpl() {
    }
    
    
    // Customer segment state change methods
    /**----------------------------------------------------------
     * Adds the Customer to the broker model
     */
    public void addCustomer(Customer cust) throws BrokerException{
	    try {
	    	em.persist(cust);
		} catch (EntityExistsException exe) {
			throw new BrokerException("Duplicate customer : " + cust.getId());
	    }
    }

    /**-------------------------------------------------------------
     * deletes the customer from the broker model
     */
    public void deleteCustomer(Customer cust)
            throws BrokerException {
    	String id = cust.getId();
    	cust = em.find(Customer.class, id);
    	if (cust == null) {
    		throw new BrokerException("Cust : " + cust.getId() + " not found");
    	} else {
    		em.remove(cust);
    	}
    }

    /**-------------------------------------------------------------
     * Updates the customer in the broker model
     */
    public void updateCustomer(Customer cust)
            throws BrokerException {
        Customer cr = getCustomer(cust.getId());
        cr.setAddr(cust.getAddr());
        cr.setName(cust.getName());
    }

    // Customer segment state query methods
    /**-------------------------------------------------------------
     * Given an id, returns the Customer from the model
     */
    public Customer getCustomer(String id)
            throws BrokerException {
    	Customer cust = em.find(Customer.class, id);
    	if (cust == null) {
    		throw new BrokerException("Customer : " + id + " not found");
    	} else {
    		return cust;
    	}
    }

    /**-------------------------------------------------------------
     * Returns all customers in the broker model
     */
    public Customer[] getAllCustomers()
            throws BrokerException {
        Query query = em.createNativeQuery("SELECT * FROM CUSTOMER", Customer.class);
        List custs = query.getResultList();
        return (Customer[]) custs.toArray(new Customer[0]);
    }

    public CustomerShare[] getAllCustomerShares(String customerId) throws BrokerException {
    	Query query = em.createNativeQuery("SELECT * FROM SHARES", CustomerShare.class);
    			List custShs = query.getResultList();
    			return (CustomerShare[]) custShs.toArray(new CustomerShare[0]);
    }

    public void addCustomerShare(CustomerShare cs) throws BrokerException {
    	try {
    		em.persist(cs);
		} catch (EntityExistsException exe) {
    		throw new BrokerException("Duplicate CustomerShare : " + cs.getId());
		}
    }

    public void updateCustomerShare(CustomerShare cs) throws BrokerException {
    	CustomerShare s = em.find(CustomerShare.class, cs.getId());
    	if (s == null) {
    		throw new BrokerException("CustomerShare : " + cs.getId() + " not found");
    	} else {
    		em.merge(cs);
    	}
    }

    public Stock[] getAllStocks() throws BrokerException {
    	Query query = em.createNativeQuery("SELECT * FROM STOCK", Stock.class);
    	List stocks = query.getResultList();
    	return (Stock[])stocks.toArray(new Stock[0]);
    }

    public Stock getStock(String symbol) throws BrokerException {
    	Stock stock = em.find(Stock.class, symbol);
    	if (stock == null) {
    		throw new BrokerException("Stock : " + symbol + " not found");
    	} else {
    		return stock;
    	}
    }

    public void addStock(Stock stock) throws BrokerException {
    	try {
    			em.persist(stock);
    		} catch (EntityExistsException exe) {
    			throw new BrokerException("Duplicate Stock : " + stock.getSymbol());
    		}
	}

    public void updateStock(Stock stock) throws BrokerException {
    	Stock s = em.find(Stock.class, stock.getSymbol());
    	if (s == null) {
    		throw new BrokerException("Stock : " + stock.getSymbol() + " not found");
    	} else {
    		em.merge(stock);
    	}
	}

    public void deleteStock(Stock stock) throws BrokerException {
    	String id = stock.getSymbol();
    	stock = em.find(Stock.class, id);
    	if (stock == null) {
    		throw new BrokerException("Stock : " + stock.getSymbol() + " not found");
    	} else {
    		em.remove(stock);
    	}
	}
}
