package servlet;

import java.io.Serializable;

import javax.persistence.*;


@Entity
@Table(name="STOCK")
public class Stock implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id @Column(name="SYMBOL") private String symbol;
	@Column(name="PRICE")private double price;
	
	
	public Stock(){
		//Default
	}

    public Stock(String symbol, double price) {
        this.symbol = symbol;
        this.price = price;
    }

    // Methods to return the private values of this object
    public double getPrice() {
        return price;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setPrice(double newPrice) {
        price = newPrice;
    }

    public String toString() {
        return "Stock:  " + symbol + "  " + price;
    }
}
